﻿using System;

namespace Framework.Processo.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var numeroValido = false;

            while (!numeroValido)
            {
                try
                {
                    Console.WriteLine("Digite um número inteiro: ");
                    string entrada = Console.ReadLine();

                    numeroValido = Int32.TryParse(entrada, out int numero);

                    if (!numeroValido)
                    {
                        Console.WriteLine($"{entrada} não é um número inteiro válido.");
                        continue;
                    }

                    var retorno = Aplicacao.Calculadores.NumeroInteiro.Calcular(numero);

                    Console.WriteLine($"Número de Entrada: {numero}");
                    Console.WriteLine($"Números divisores: {string.Join(' ', retorno.Divisores)}");
                    Console.WriteLine($"Números Primos: {string.Join(' ', retorno.Primos)}");

                    Console.ReadKey();
                }
                catch
                {
                    Console.WriteLine("Ops! Ocorreu um erro. Tente mais tarde.");
                    throw;
                }
            }
        }
    }
}
