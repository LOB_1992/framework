﻿using Microsoft.AspNetCore.Mvc;

namespace Framework.Processo.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DivisoresController : ControllerBase
    {
        [HttpGet("{numero}")]
        public IActionResult ObterDivisores(int numero)
        {
            var retorno = Aplicacao.Calculadores.NumeroInteiro.Calcular(numero);
            return Ok(retorno);
        }
    }
}
