﻿namespace Framework.Processo.Dominio.Identificadores
{
    public static class Primo
    {
        public static bool EPrimo(int numero)
        {
            if (numero < 2) // 1 não é considerado número primo
                return false;

            for(int i = 2; i <= (numero / 2); i++)
            {
                if (Uteis.Divisores.EDivisor(numero, i))
                    return false;
            }

            return true;
        }
    }
}
