﻿using System.Collections.Generic;

namespace Framework.Processo.Aplicacao.Dtos
{
    public class ResultadoNumeroInteiro
    {
        public List<int> Divisores { get; set; } = new List<int>();
        public List<int> Primos { get; set; } = new List<int>();
    }
}
