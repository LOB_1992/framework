﻿using Framework.Processo.Aplicacao.Dtos;
using Framework.Processo.Dominio.Uteis;

namespace Framework.Processo.Aplicacao.Calculadores
{
    public static class NumeroInteiro
    {
        public static ResultadoNumeroInteiro Calcular(int numero)
        {
            var resultado = new ResultadoNumeroInteiro();

            for(int i= 1; i<= numero; i++)
            {
                if (Divisores.EDivisor(numero, i))
                {
                    resultado.Divisores.Add(i);

                    if (Dominio.Identificadores.Primo.EPrimo(i))
                        resultado.Primos.Add(i);
                }
            }

            return resultado;
        }
    }
}
