﻿using System.Linq;
using Xunit;

namespace Framework.Processo.Testes.Aplicacao
{
    public class NumeroInteiroTestes
    {
        [Theory]
        [InlineData(45)]
        [InlineData(100)]
        [InlineData(2)]
        public void Deve_Retornar_Divisores_E_Primos(int numero)
        {
            var retorno = Framework.Processo.Aplicacao.Calculadores.NumeroInteiro.Calcular(numero);

            Assert.True(retorno.Divisores.Any());
            Assert.True(retorno.Primos.Any());
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Não_Deve_Retornar_Divisores_E_Primos(int numero)
        {
            var retorno = Framework.Processo.Aplicacao.Calculadores.NumeroInteiro.Calcular(numero);

            Assert.False(retorno.Divisores.Any());
            Assert.False(retorno.Primos.Any());
        }

        [Theory]
        [InlineData(17)]
        [InlineData(2147483647)] //int.MaxValue
        public void Deve_Retornar_Um_Numero_Em_Primos(int numero)
        {
            var retorno = Framework.Processo.Aplicacao.Calculadores.NumeroInteiro.Calcular(numero);

            Assert.True(retorno.Primos.Count == 1);
            Assert.Contains(numero, retorno.Primos);
        }
    }
}
