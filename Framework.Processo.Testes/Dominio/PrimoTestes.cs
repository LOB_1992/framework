﻿using Xunit;

namespace Framework.Processo.Testes.Dominio
{
    public class PrimoTestes
    {
        [Theory]
        [InlineData(2)]
        [InlineData(2147483647)] // int.MaxValue
        [InlineData(11)]
        public void Deve_Retornar_Verdadeiro(int numero)
        {
            var ePrimo = Processo.Dominio.Identificadores.Primo.EPrimo(numero);

            Assert.True(ePrimo);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(0)]
        [InlineData(-12)]
        public void Deve_Retornar_Falso(int numero)
        {
            var ePrimo = Processo.Dominio.Identificadores.Primo.EPrimo(numero);

            Assert.False(ePrimo);
        }
    }
}
