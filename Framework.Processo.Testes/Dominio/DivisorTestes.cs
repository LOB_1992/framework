﻿using Xunit;

namespace Framework.Processo.Testes.Dominio
{
    public class DivisorTestes
    {
        [Theory]
        [InlineData(4, 2)]
        [InlineData(4, 4)]
        [InlineData(0, 8)]
        public void Deve_Retornar_Verdadeiro(int divisor, int dividendo)
        {
            var eDivisor = Processo.Dominio.Uteis.Divisores.EDivisor(divisor, dividendo);

            Assert.True(eDivisor);
        }

        [Theory]
        [InlineData(7, 3)]
        [InlineData(-5, 2)]
        [InlineData(3, 4)]
        public void Deve_Retornar_Falso(int divisor, int dividendo)
        {
            var eDivisor = Processo.Dominio.Uteis.Divisores.EDivisor(divisor, dividendo);

            Assert.False(eDivisor);
        }
    }
}
